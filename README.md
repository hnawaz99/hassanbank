# HassanBank

## How to compile?
```bash
# Go into the project source folder (src)
$ cd HassanBank/src

# Compile the main class
$ javac App.java

# Run the main
$ java App
```
