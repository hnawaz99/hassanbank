
class Account {
  int id;
  String name;
  float balance; //euros

  Account(int id, String name, float balance) {
    this.id = id;
    this.name = name;
    this.balance = balance;
  }
}
