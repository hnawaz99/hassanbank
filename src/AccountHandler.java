
import java.util.*;
import java.io.*;

class AccountHandler {

  ArrayList<Account> array = new ArrayList<Account>();

  void mostrararray() {
    for (Account f : array) {
      System.out.println("Account no : " + f.id);
      System.out.println("Account name : " + f.name);
      System.out.println("Account balance : " + f.balance);
      System.out.println("--------------------------");
    }
  }

  void guardararray(File file) {
    try {
      BufferedWriter outputStream = new BufferedWriter(new FileWriter(file, false));

      for (Account f : array) {
        outputStream.write(f.id+":"+f.name+":"+f.balance+"\n");
      }

      outputStream.close();

    } catch(Exception e) {
      System.out.println(e.getMessage());
    }
  }

  void initializearray(File file) {

    try {
      BufferedReader inputStream = new BufferedReader(new FileReader(file));
      String line = "";

      while((line = inputStream.readLine()) != null) {

        String[] parts = line.split(":");

        Account f = new Account(Integer.parseInt(parts[0]), parts[1], Float.parseFloat(parts[2]));

        array.add(f);
      }

      inputStream.close();

    } catch(Exception e) {
      System.out.println(e.getMessage());
    }

  }

}
