
import java.util.*;
import java.io.*;
import java.util.Scanner;

public class App {
	public static void main(String[] args)  throws IOException {
		Scanner myObj = new Scanner(System.in);
		AccountHandler handler = new AccountHandler();

		handler.initializearray(new File("accounts.data"));
		handler.mostrararray();

		System.out.println();
		System.out.println();

		System.out.println("Welcome To Hassan Bank Limited (HBL) Users list.  Do you want to add new Account? (Enter Y to procceed.)");
		char answer1 = myObj.next().charAt(0);
		if (answer1 == 'y' || answer1 == 'Y') {
			System.out.println("Enter Id for account. Id Must be in four (4) numbers!!");
			int id = myObj.nextInt();
			myObj.nextLine();
			System.out.println("Enter Your Full  Name for account Please ");
			String name = myObj.nextLine();
			System.out.println("Enter Balance for account");
			float balance = myObj.nextFloat();
			handler.array.add(new Account(id, name, balance));
			System.out.println("New Account has been added.");
			System.out.println();
			System.out.println("Do you want to see the list again? (Enter Y to procceed.)");
			char answer2 = myObj.next().charAt(0);
			if (answer2 == 'y' || answer2 == 'Y'){
				handler.mostrararray();
			}
		}
		else{
			System.out.println("No new accounts has been added");
		}

		handler.guardararray(new File("accounts.data"));
	}
}
